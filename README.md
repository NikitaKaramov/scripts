# youtube

Сценарии и дополнительные материалы к видео YouTube-канала «Никита Карамов»

## Содержание

* [Channel Artwork](./Channel%20Artwork) — фирменный стиль канала
  * [Banner](./Channel%20Artwork/Banner) — баннер канала
  * [Logo](./Channel%20Artwork/Logo) — логотип канала
* [Video](./Video) — материалы к видео

## Лицензия

Опубликованные материалы доступны на условиях [лицензии Creative Commons
«Attribution» («Атрибуция») 4.0 Всемирная][cc-by-4.0].

[![CC-BY-4.0](https://img.shields.io/badge/%20-BY%204.0-black?style=for-the-badge&logo=creativecommons&logoColor=white)][cc-by-4.0]

[cc-by-4.0]: http://creativecommons.org/licenses/by/4.0/
